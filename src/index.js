import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter as Router, Route, } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker';
import App from './components/App';
import UserInformation from './components/UserInformation'

ReactDOM.render(
  <Router>
    <div>
      <Route exact path="/" component={App}/>
      {/* <Route exact path='/' component={UserInformation} /> */}
    </div>
  </Router>,
  document.getElementById('root'));

registerServiceWorker();
