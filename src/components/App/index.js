import React, { Component } from 'react';
import './App.css';
import UserInformation from '../UserInformation';
import FbLogin from '../Facebook/Login'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected: [],
    };
  }

  handleChange = (e) => {
    const { checked, id }  = e.target;
    const { selected } = this.state;

    if (checked === false){
      const selectedSpots = new Set(selected);
      selectedSpots.delete(parseInt(id, 10));
      return this.setState({selected: selectedSpots})
    } else {
      this.setState({
        selected: [...selected, parseInt(e.target.id, 10)]
      })
    }
}

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Booking System</h2>
          <FbLogin />
        </div>

        <div>
          <div className="booking_selection">
            <input type="checkbox" id="1" onChange={this.handleChange} name="booking" />
            <input type="checkbox" id="2" onChange={this.handleChange} name="booking" />
            <input type="checkbox" id="3" onChange={this.handleChange} name="booking" />
            <button className="button" onClick={() => console.log('Reserved Seats', this.state.selected)}>Next</button>
          </div>
          <div className="user_information">
            <UserInformation seatSelected={this.state.selected}/>
          </div>
        </div>

      </div>
    );
  }
}

export default App;
