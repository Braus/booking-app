import React, { Component } from 'react';

class UserInformation extends Component {
  constructor(props) {
    super(props);
  }

  render() {

    const { seatSelected } = this.props;

    return (
      <div>
        <h2>User Information</h2>
        <h3>Seat Selections: {seatSelected} </h3>
      </div>
    );
  }

}

export default UserInformation;
