import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login';
import { Redirect } from 'react-router-dom'

class FbLogin extends Component {

  constructor(props) {
    super(props);
    this.state = {
      logged: false,
      fbId: '',
      name: '',
      email: ''
    };
  }

  render(){
    const responseFacebook = (response) => {
      this.setState({
        logged: true,
        fbId: response.id,
        name: response.name
      })
    }



    if (this.state.logged){
      return <Redirect to={this.state.fbId} />;
    }

    return <FacebookLogin
      appId="753860174795573"
      autoLoad={true}
      fields="name,email,picture"
      callback={responseFacebook} />
  }
}

export default FbLogin;
